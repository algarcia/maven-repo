# Ángel L. García maven repository

Welcome to my public artifacts repository.

## Use it with **maven**

Add the following snippet to your **pom.xml**:
  
```
<repositories>
    <repository>
        <id>algarcia-bitbucket</id>
        <name>Angel L. Garcia maven repository</name>
        <url>https://bitbucket.org/algarcia/maven-repo/raw/master/</url>
        <layout>default</layout>
    </repository>
</repositories>
```

## Available Artifacts

